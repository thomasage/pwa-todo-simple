<?php
declare(strict_types=1);

ini_set('display_errors', 'On');
error_reporting(-1);

// Simulate slow server
sleep(2);

$data = json_decode(file_get_contents(__DIR__.'/../tasks.json'), true, 512, JSON_THROW_ON_ERROR);

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    header('Content-Type: application/json');
    echo json_encode($data);
    exit();
}

if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['task'])) {
    $id = 1;
    $ids = array_column($data, 'id');
    if (count($ids) > 0) {
        $id = max($ids) + 1;
    }
    $data[] = [
        'id' => $id,
        'title' => $_POST['taks'],
    ];
    http_response_code(201);
    exit();
}

http_response_code(400);
echo 'Bad Request';

