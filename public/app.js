const CACHE_NAME = 'V14'

if ('serviceWorker' in navigator) {
  navigator.serviceWorker
    .register('/service-worker.js', { scope: '/' })
    .then(registration => {
      console.debug('SW ready, scope is', registration.scope)
    })
    .catch(error => {
      console.error('SW failed', error)
    })
} else {
  console.error('SW not supported')
}

taskLoad()

async function taskLoad () {
  console.debug('CALL taskLoad')
  const request = new Request('/index.php')
  const cache = await caches.open(CACHE_NAME)
  const responseCache = await cache.match(request)
  if (responseCache) {
    console.debug('taskLoad CACHE MATCH')
    console.debug('taskLoad RENDER FROM CACHE')
    taskRenderAll(await responseCache.json())
  } else {
    console.debug('taskLoad CACHE EMPTY')
  }
  console.debug('taskLoad REQUEST NETWORK')
  const responseNetwork = await fetch(request)
  console.debug('taskLoad STORE IN CACHE')
  await cache.put(request, responseNetwork.clone())
  console.debug('taskLoad RENDER FROM NETWORK')
  taskRenderAll(await responseNetwork.json())
}

// document.getElementById('tasks').addEventListener('click', async event => {
//   if (event.target.closest('#tasks .action-delete')) {
//     await taskDelete(event)
//   }
// })
// document.getElementById('action-cache-drop').addEventListener('click', cacheDrop)
// document.getElementById('action-load').addEventListener('click', taskLoad)
// document.getElementById('form-add').addEventListener('submit', taskAdd)
//
// let cache
//
// // init()
//
// async function init () {
//   cache = await caches.open(CACHE_VERSION)
//   await taskLoad()
// }
//
// async function cacheDrop (event) {
//   console.debug('cacheDrop INIT')
//   event.preventDefault()
//   await caches.delete(CACHE_VERSION)
//   console.debug('cacheDrop DONE')
// }
//
// async function taskAdd (event) {
//   console.debug('taskAdd INIT')
//   event.preventDefault()
//   const form = event.target
//   await fetch('/index.php', {
//     body: new FormData(form),
//     method: 'POST'
//   })
//   await taskLoad()
// }
//
// async function taskDelete (event) {
//   console.debug('taskDelete INIT')
//   event.preventDefault()
//   const id = event.target.closest('[data-id]').dataset.id
//   await fetch(`/index.php?id=${id}`, {
//     method: 'DELETE'
//   })
//   await taskLoad()
// }
//
// async function taskLoad () {
//   console.debug('taskLoad INIT')
//   const request = new Request('/index.php')
//   console.debug('taskLoad REQUEST CACHE')
//   const responseCache = await cache.match(request)
//   if (responseCache) {
//     console.debug('taskLoad CACHE MATCH')
//     console.debug('taskLoad RENDER FROM CACHE')
//     taskRenderAll(await responseCache.json())
//   } else {
//     console.debug('taskLoad CACHE EMPTY')
//   }
//   console.debug('taskLoad REQUEST NETWORK')
//   const responseNetwork = await fetch(request)
//   console.debug('taskLoad STORE IN CACHE')
//   cache.put(request, responseNetwork.clone())
//   console.debug('taskLoad RENDER FROM NETWORK')
//   taskRenderAll(await responseNetwork.json())
// }

function taskRenderAll (tasks) {
  const list = document.getElementById('tasks')
  list.innerHTML = 'Loading...'
  list.innerHTML = ''
  tasks.forEach(item => {
    list.innerHTML += `<div data-id="${item.id}">${item.title} <button type="button" style="float:right;" class="action-delete">D</button></div>`
  })
}
