const CACHE_NAME = 'V14'
const PRECACHED = [
  '/app.css',
  '/app.js',
  '/favicon.ico',
  '/front.html',
  '/manifest.json',
  '/icon/192.png',
  '/icon/512.png'
]

self.addEventListener('install', event => {
  console.debug('EVENT install')
  event.waitUntil(cacheWarmUp())
})

self.addEventListener('fetch', event => {
  const request = event.request
  console.debug(`EVENT fetch for ${request.url}`)
  if (request.cache === 'only-if-cached' && request.mode !== 'same-origin') {
    console.debug(`event fetch ignored for request ${request.url}`)
    return
  }
  const url = new URL(request)
  if (PRECACHED.indexOf(url.pathname) === -1) {
    console.debug(`event fetch ignored for request ${request.url}`)
  }
  console.debug(`event fetch handled for request ${request.url}`)
  event.respondWith(cacheFetch(request))
  event.waitUntil(cacheUpdate(request))
})

self.addEventListener('activate', event => {
  console.debug('EVENT activate')
  event.waitUntil(cachePurgeOutdated())
})

async function cacheFetch (request) {
  console.debug(`CALL cacheFetch for ${request.url}`)
  const cache = await caches.open(CACHE_NAME)
  const response = await cache.match(request)
  if (!response) {
    console.debug('CACHE miss')
    return cacheUpdate(request)
  }
  console.debug('CACHE hit')
  console.debug(`Response for ${request.url}`)
  return Promise.resolve(response)
}

function cacheUpdate (request) {
  console.debug(`CALL update for ${request.url}`)
  return caches
    .open(CACHE_NAME)
    .then(cache =>
      fetch(request)
        .then(async response => {
          await cache.put(request, response.clone())
          return response
        })
    )
}

async function cachePurgeOutdated () {
  console.debug('CALL cachePurgeOutdated')
  const cacheNames = await caches.keys()
  await Promise.all(
    cacheNames
      .filter(cacheName => cacheName !== CACHE_NAME)
      .map(cacheName => {
        console.debug(`Purge cache ${cacheName}`)
        return caches.delete(cacheName)
      })
  )
}

async function cacheWarmUp () {
  console.debug('CALL cacheWarmUp')
  const cache = await caches.open(CACHE_NAME)
  await cache.addAll(PRECACHED)
}
